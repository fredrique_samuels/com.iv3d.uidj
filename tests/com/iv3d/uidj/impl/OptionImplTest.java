/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package com.iv3d.uidj.impl;

import com.iv3d.common.utils.JsonUtils;
import com.iv3d.uidj.Option;

import junit.framework.TestCase;

public class OptionImplTest extends TestCase {
	
	public void testConstructor() {
		Option param = new OptionImpl("id", "value");
		assertEquals("id", param.getId());
		assertEquals("value", param.getValue());
	}
	
	public void testToJson() {
		Option param = new OptionImpl("code", "1234");
		String json = JsonUtils.writeToString(param);
		
		String expected = "{\"id\":\"code\",\"value\":\"1234\"}";
		assertEquals(expected, json);
	}
	
	public void testReadFromJson() {
		String expected = "{\"id\":\"source\",\"value\":\"dest\"}";
		Option param = JsonUtils.readFromString(expected, OptionImpl.class);
		
		assertEquals("source", param.getId());
		assertEquals("dest", param.getValue());
	}
}
