/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package com.iv3d.uidj.impl;

import com.iv3d.uidj.RestAction;
import com.iv3d.uidj.RestActionBuilder;
import com.iv3d.uidj.RestMethod;
import com.iv3d.uidj.RestParam;

import junit.framework.TestCase;

public class RestActionBuilderImplTest extends TestCase {
	
	public void testUrlAndMethodSet() {
		RestActionBuilder builder = new RestActionBuilderImpl("/url", RestMethod.POST);
		RestAction action = builder.build();
		
		assertNotNull(action);
		assertEquals("/url",action.getUrl());
		assertEquals(RestMethod.POST, action.getMethod());
		assertNull(null, action.getBody());
		assertNull(action.getParams());
	}
	
	public void testBodySet() {
		RestActionBuilderImpl builder = new RestActionBuilderImpl("/url", RestMethod.POST);
		RestAction action = builder.setBody("_body123")
				.build();
		
		assertNotNull(action);
		assertEquals("_body123", action.getBody());
	}
	
	public void testParamsSet() {
		RestActionBuilder builder = new RestActionBuilderImpl("/url", RestMethod.POST);
		RestAction action = builder.addParam("key1", "value1")
				.addParam("key2", "value2")
				.build();
		
		assertNotNull(action);
		
		RestParam[] params = action.getParams();
		assertNotNull(params);
		assertEquals(2, params.length);
		
		{
			RestParam param = params[0];
			assertEquals("key1", param.getName());
			assertEquals("value1", param.getValue());
		}
		
		{
			RestParam param = params[1];
			assertEquals("key2", param.getName());
			assertEquals("value2", param.getValue());
		}
	}
}
