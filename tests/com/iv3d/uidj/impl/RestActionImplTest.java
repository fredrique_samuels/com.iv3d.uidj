/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package com.iv3d.uidj.impl;

import com.iv3d.common.utils.JsonUtils;
import com.iv3d.uidj.RestAction;
import com.iv3d.uidj.RestMethod;
import com.iv3d.uidj.RestParam;

import junit.framework.TestCase;

public class RestActionImplTest extends TestCase {
	
	public void testUrlToJson() {
		RestAction instance = new RestActionImpl().setUrl("/url");
		
		String actual = JsonUtils.writeToString(instance);
		assertEquals("{\"url\":\"/url\"}", actual);
	}
	
	public void testSetUrl() {
		RestAction instance = new RestActionImpl().setUrl("/url");
		assertEquals("/url", instance.getUrl());
	}
	
	public void testSetMethod() {
		RestAction instance = new RestActionImpl().setMethod(RestMethod.GET);
		assertEquals(RestMethod.GET, instance.getMethod());
	}
	
	public void testSetMethodGetToJson() {
		RestAction instance = new RestActionImpl().setMethod(RestMethod.GET);
		
		String actual = JsonUtils.writeToString(instance);
		assertEquals("{\"method\":\"GET\"}", actual);
	}
	
	public void testSetMethodPostToJson() {
		RestAction instance = new RestActionImpl().setMethod(RestMethod.POST);
		
		String actual = JsonUtils.writeToString(instance);
		assertEquals("{\"method\":\"POST\"}", actual);
	}
	
	public void testSetParams() {
		RestAction instance = new RestActionImpl()
				.addParam("param1", "value1");
		RestParam[] params = instance.getParams();
		assertEquals(1,  params.length);
		
		RestParam param = params[0];
		assertTrue(param instanceof RestParamImpl);
		assertEquals("param1", param.getName());
		assertEquals("value1", param.getValue());		
	}
	
	public void testSetMultipleParams() {
		RestAction instance = new RestActionImpl()
				.addParam("paramA", "valueA")
				.addParam("paramB", "valueB");
		
		RestParam[] params = instance.getParams();
		assertEquals(2,  params.length);
		
		{
			RestParam param = params[0];
			assertTrue(param instanceof RestParamImpl);
			assertEquals("paramA", param.getName());
			assertEquals("valueA", param.getValue());
		}
		
		{
			RestParam param = params[1];
			assertTrue(param instanceof RestParamImpl);
			assertEquals("paramB", param.getName());
			assertEquals("valueB", param.getValue());
		}
	}
	
	public void testParamsToJsonWhenPopulated() {
		RestAction instance = new RestActionImpl()
				.addParam("paramA", "valueA")
				.addParam("paramB", "valueB");

		String actual = JsonUtils.writeToString(instance);
		assertEquals("{\"params\":[{\"name\":\"paramA\",\"value\":\"valueA\"},{\"name\":\"paramB\",\"value\":\"valueB\"}]}", actual);
	}
	
	public void testSetBody() {
		RestAction instance = new RestActionImpl()
				.setBody("{\"hello\":\"bye\"}");
		String body = instance.getBody();
		assertEquals("{\"hello\":\"bye\"}", body);
	}
	
	public void testBodyToJson() {
		RestAction instance = new RestActionImpl()
			.setBody("{\"hello\":\"bye\"}");
	
		String actual = JsonUtils.writeToString(instance);
		assertEquals("{\"body\":\"{\\\"hello\\\":\\\"bye\\\"}\"}", actual);
	}
	
	public void testReadUrlFromJson() {
		RestAction actual = JsonUtils.readFromString("{\"url\":\"/url\"}", RestActionImpl.class);
		assertEquals("/url", actual.getUrl());
	}
	
	public void testReadMethodFromJson() {
		RestAction actual = JsonUtils.readFromString("{\"method\":\"GET\"}", RestActionImpl.class);
		assertEquals(RestMethod.GET, actual.getMethod());
	}
	
	public void testReadParamsFromJson() {
		String json = "{\"params\":[{\"name\":\"paramA\",\"value\":\"valueA\"},{\"name\":\"paramB\",\"value\":\"valueB\"}]}";
		RestAction actual = JsonUtils.readFromString(json, RestActionImpl.class);
		
		RestParam[] params = actual.getParams();
		assertEquals(2, params.length);
		
		{
			RestParam param = params[0];
			assertTrue(param instanceof RestParamImpl);
			assertEquals("paramA", param.getName());
			assertEquals("valueA", param.getValue());
		}
		
		{
			RestParam param = params[1];
			assertTrue(param instanceof RestParamImpl);
			assertEquals("paramB", param.getName());
			assertEquals("valueB", param.getValue());
		}
	}
	
	public void testReadBodyFromJson() {
		String json = "{\"body\":\"{\\\"hello\\\":\\\"bye\\\"}\"}";
		RestAction actual = JsonUtils.readFromString(json, RestActionImpl.class);
		
		String body = actual.getBody();
		assertEquals("{\"hello\":\"bye\"}", body);
	}
	
	public void testSetId(){
		RestAction instance = (RestActionImpl)(new RestActionImpl().setId("action1"));
		assertEquals("action1", instance.getId());
	}
	
	public void testIdToJson() {
		RestAction instance = (RestActionImpl)(new RestActionImpl().setId("action1"));
		String json = JsonUtils.writeToString(instance);
		assertEquals("{\"id\":\"action1\"}", json);
	}
	
	public void testValidationUrlAndMethodAbsenceErrors() {
		RestAction instance = new RestActionImpl();
		String[] errors = instance.validate();
		assertEquals(2, errors.length);
		
		assertEquals("action.url not specified.", errors[0]);
		assertEquals("action.method not specified.", errors[1]);
	}
	
	public void testValidationUrlAbsenceError() {
		RestAction instance = new RestActionImpl()
				.setMethod(RestMethod.GET);
		String[] errors = instance.validate();
		
		assertEquals(1, errors.length);
		assertEquals("action.url not specified.", errors[0]);
	}
	
	public void testValidationMethodAbsenceError() {
		RestAction instance = new RestActionImpl()
				.setUrl("/url");
		String[] errors = instance.validate();
		
		assertEquals(1, errors.length);
		assertEquals("action.method not specified.", errors[0]);
	}
	
	public void testUrlFormatError() {
		RestAction instance = new RestActionImpl()
				.setMethod(RestMethod.GET)
				.setUrl("::??");
		String[] errors = instance.validate();
		
		assertEquals(1, errors.length);
		assertEquals("action.url '::??' not in valid format.", errors[0]);
	}
	
}
