/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package com.iv3d.uidj.impl;

import com.iv3d.common.utils.JsonUtils;
import com.iv3d.uidj.DataType;
import com.iv3d.uidj.Input;
import com.iv3d.uidj.InputType;
import com.iv3d.uidj.Option;

import junit.framework.TestCase;
import mockit.Mocked;
import mockit.NonStrictExpectations;

public class InputImplTest extends TestCase {
	
	@Mocked 
	DataType<Input> mockedDataType;
	
	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	@Override
	protected void setUp() throws Exception {
		super.setUp();

		new NonStrictExpectations() {
			
			{
				mockedDataType.getKey();
				result = "mocked_type";
			}
		};
	}

	private InputImpl getInstance() {
		return new InputImpl();
	}
	
	public void testNoOptionsToJson() {
		InputImpl instance = getInstance();
		String json = JsonUtils.writeToString(instance);
		assertEquals("{}", json);
	}
	
	public void testSetOptions() {
		InputImpl instance = getInstance();
		String id = "optionId";
		String value = "optionValue";
		
		instance.addOption(id, value);
		
		Option[] options = instance.getOptions();
		assertNotNull(options);
		assertEquals(1, options.length);
		
		Option option = options[0];
		assertEquals("optionId", option.getId());
		assertEquals("optionValue", option.getValue());
	}
		
	public void testOptionsToJson() {
		InputImpl instance = getInstance();
		instance.addOption("optionId1", "optionValue1");
		
		String json = JsonUtils.writeToString(instance);
		assertEquals("{\"options\":[{\"id\":\"optionId1\",\"value\":\"optionValue1\"}]}", json);
	}
	
	public void testMultipleOptionsToJson() {
		InputImpl instance = getInstance();
		instance.addOption("optionId1", "optionValue1");
		instance.addOption("optionId2", "optionValue2");
		
		String json = JsonUtils.writeToString(instance);
		assertEquals("{\"options\":[{\"id\":\"optionId1\",\"value\":\"optionValue1\"},{\"id\":\"optionId2\",\"value\":\"optionValue2\"}]}", json);
	}
	
	public void testMultipleOptionsFromJson() {
		
		String json = "{\"options\":[{\"id\":\"optionId1\",\"value\":\"optionValue1\"},{\"id\":\"optionId2\",\"value\":\"optionValue2\"}]}";
		Input input = JsonUtils.readFromString(json, InputImpl.class);
		
		Option[] options = input.getOptions();
		assertEquals(2, options.length);
		
		{
			Option option = options[0];
			assertEquals("optionId1", option.getId());
			assertEquals("optionValue1", option.getValue());
		}
		{
			Option option = options[1];
			assertEquals("optionId2", option.getId());
			assertEquals("optionValue2", option.getValue());
		}
	}
	
	public void testSetType() {
		InputImpl instance = getInstance();
		instance.setType(mockedDataType);
		
		assertEquals(mockedDataType, instance.getType());
	}
	
	public void testTypeToJson() {
		InputImpl instance = getInstance();
		instance.setType(mockedDataType);
		
		String json = JsonUtils.writeToString(instance);
		assertEquals("{\"type\":\"mocked_type\"}", json);
	}
	
	public void testTypeFromJson() {
		String json = "{\"type\":\"html\"}";
		InputImpl param = JsonUtils.readFromString(json, InputImpl.class);
		assertEquals(InputType.HTML, param.getType());
	}
}
