/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.uidj.impl;

import com.iv3d.uidj.Input;
import com.iv3d.uidj.InputBuilder;
import com.iv3d.uidj.InputType;
import com.iv3d.uidj.Option;

import junit.framework.TestCase;

/**
 * @author fred
 *
 */
public class InputBuilderImplTest extends TestCase {
	
	
	public void testCreate() {
		InputBuilder builder = new InputBuilderImpl("MyId", InputType.TEXT);
		Input input = builder.build();
		
		assertNotNull(input);
		assertEquals("MyId", input.getId());
		assertEquals(InputType.TEXT, input.getType());
		assertNull(input.getName());
		assertFalse(input.isHidden());
	}
	
	public void testSetName() {
		InputBuilder builder = new InputBuilderImpl("id", InputType.FILE);
		builder = builder.setName("TestName");
		Input input = builder.build();
		
		assertNotNull(input);
		assertEquals("id", input.getId());
		assertEquals(InputType.FILE, input.getType());
		assertEquals("TestName", input.getName());
		assertFalse(input.isHidden());
	}
	
	public void testSetHidden() {
		InputBuilder builder = new InputBuilderImpl("id", InputType.FILE);
		builder = builder.setHidden();
		Input input = builder.build();
		
		assertNotNull(input);
		assertEquals("id", input.getId());
		assertEquals(InputType.FILE, input.getType());
		assertTrue(input.isHidden());
	}
	
	public void testDefaultOptions() {
		InputBuilder builder = new InputBuilderImpl("id", InputType.FILE);
		Input input = builder.build();		
		assertNull(input.getOptions());
	}
	
	public void testAddOption() {
		InputBuilder builder = new InputBuilderImpl("id", InputType.FILE);
		builder = builder.addOption("id", "value");
		Input input = builder.build();
		
		Option[] options = input.getOptions();
		assertNotNull(options);
		assertEquals(1, options.length);
		
		{
			Option option = options[0];
			assertEquals("id", option.getId());
			assertEquals("value", option.getValue());
		}
	}

	public void testAddMultipleOption() {
		InputBuilder builder = new InputBuilderImpl("id", InputType.FILE);
		builder = builder.addOption("id1", "value1");
		builder = builder.addOption("id2", "value2");
		Input input = builder.build();
		
		Option[] options = input.getOptions();
		assertNotNull(options);
		assertEquals(2, options.length);
		
		{
			Option option = options[0];
			assertEquals("id1", option.getId());
			assertEquals("value1", option.getValue());
		}
		{
			Option option = options[1];
			assertEquals("id2", option.getId());
			assertEquals("value2", option.getValue());
		}
	}

}
