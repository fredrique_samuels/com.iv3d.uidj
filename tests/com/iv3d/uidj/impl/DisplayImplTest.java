/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.uidj.impl;

import com.iv3d.common.utils.JsonUtils;
import com.iv3d.uidj.DataType;
import com.iv3d.uidj.Display;
import com.iv3d.uidj.DisplayType;
import com.iv3d.uidj.RestAction;
import com.iv3d.uidj.RestMethod;

import junit.framework.TestCase;
import mockit.Mocked;
import mockit.NonStrictExpectations;

/**
 * @author fred
 *
 */
public class DisplayImplTest extends TestCase {
	@Mocked 
	DataType<Display> mockedDataType;
	@Mocked
	RestActionImpl action;
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();

		new NonStrictExpectations() {
			
			{
				mockedDataType.getKey();
				result = "mocked_type";
			}
		};
	}
	
	private DisplayImpl getInstance() {
		return new DisplayImpl();
	}
	
	public void testSetType() {
		DisplayImpl instance = getInstance();
		instance.setType(mockedDataType);
		
		assertEquals(mockedDataType, ((Display)instance).getType());
	}
	
	public void testTypeToJson() {
		DisplayImpl instance = getInstance();
		instance.setType(mockedDataType);
		
		String json = JsonUtils.writeToString(instance);
		assertEquals("{\"type\":\"mocked_type\"}", json);
	}
	
	public void testTypeFromJson() {
		String json = "{\"type\":\"html\"}";
		DisplayImpl param = JsonUtils.readFromString(json, DisplayImpl.class);
		assertEquals(DisplayType.HTML,((Display) param).getType());
	}
	
	public void testStringValueToJson() {
		DisplayImpl instance = getInstance();
		instance.setStringValue("value");
		
		String json = JsonUtils.writeToString(instance);
		assertEquals("{\"stringValue\":\"value\"}", json);
	}
	
	public void testStringValueFromJson() {
		String json = "{\"stringValue\":\"readValue\"}";
		Display display = JsonUtils.readFromString(json, DisplayImpl.class);
		
		assertEquals("readValue", display.getStringValue());
	}
	
	public void testActionToJson() {
		new NonStrictExpectations() {
			{
				action.getUrl();
				result = "/url";
				
				action.getMethod();
				result = RestMethod.GET;
				
				action.getParams();
				result = null;
			}
		};
		
		DisplayImpl instance = getInstance();
		instance.setAction(action);
		
		String json = JsonUtils.writeToString(instance);
		assertEquals("{\"action\":{\"url\":\"/url\",\"method\":\"GET\"}}", json);
	}
	
	public void testActionFromJson() {
		String json = "{\"action\":{\"url\":\"/url\",\"method\":\"GET\"}}";
		Display display = JsonUtils.readFromString(json, DisplayImpl.class);
		
		RestAction action = display.getAction();
		assertNotNull(action);
	}
	
}
