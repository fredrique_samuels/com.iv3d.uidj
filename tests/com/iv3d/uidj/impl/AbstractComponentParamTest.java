/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package com.iv3d.uidj.impl;

import com.iv3d.common.utils.JsonUtils;

import junit.framework.TestCase;

public class AbstractComponentParamTest extends TestCase {
	
	private AbstractComponentParam getInstance() {
		return new AbstractComponentParam();
	}
	
	private AbstractComponentParam getValidInstance() {
		AbstractComponentParam instance = getInstance();
		instance.setId("demoId");
		instance.setName("demoName");
		return instance;
	}
	
	public void testSetId() {
		AbstractComponentParam instance = getInstance();
		instance.setId("Id");
		
		assertEquals("Id", instance.getId());		
	}
	
	public void testIdToJson() {
		AbstractComponentParam instance = getInstance();
		instance.setId("demoId");
		
		String json = JsonUtils.writeToString(instance);
		assertEquals("{\"id\":\"demoId\"}", json);
	}
	
	public void testReadIdFromJson() {
		AbstractComponentParam instance = JsonUtils.readFromString("{\"id\":\"jsonId\"}", AbstractComponentParam.class);
		assertEquals("jsonId", instance.getId());
	}
	
	
	public void testSetName() {
		AbstractComponentParam instance = getInstance();
		instance.setName("Name");
		
		assertEquals("Name", instance.getName());		
	}

	public void testNameToJson() {
		AbstractComponentParam instance = getInstance();
		instance.setName("demoName");
		
		String json = JsonUtils.writeToString(instance);
		assertEquals("{\"name\":\"demoName\"}", json);
	}
	
	public void testReadNameFromJson() {
		AbstractComponentParam instance = JsonUtils.readFromString("{\"name\":\"jsonName\"}", AbstractComponentParam.class);
		assertEquals("jsonName", instance.getName());
	}
	
	public void testDefaultHiddenIsNull() {
		AbstractComponentParam instance = getValidInstance();
		assertNull(instance.isHidden());
	}
	
	public void testSetHidden() {
		AbstractComponentParam instance = getValidInstance();
		instance.hide();
		
		assertTrue(instance.isHidden());
	}
	
	public void testHiddenToJsonNotWritten() {
		AbstractComponentParam instance = getInstance();
		String json = JsonUtils.writeToString(instance);
		assertEquals("{}", json);
	}
	
	public void testHiddenToJson() {
		AbstractComponentParam instance = getInstance();
		instance.hide();
		
		String json = JsonUtils.writeToString(instance);
		assertEquals("{\"hidden\":true}", json);
	}
	
	public void testShowToJson() {
		AbstractComponentParam instance = getInstance();
		instance.show();
		
		String json = JsonUtils.writeToString(instance);
		assertEquals("{\"hidden\":false}", json);
	}
	
}
