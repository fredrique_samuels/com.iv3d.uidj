/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.uidj.impl;

import com.iv3d.uidj.Display;
import com.iv3d.uidj.DisplayBuilder;
import com.iv3d.uidj.DisplayType;
import com.iv3d.uidj.RestAction;
import com.iv3d.uidj.RestActionBuilder;
import com.iv3d.uidj.RestMethod;

import junit.framework.TestCase;

/**
 * @author fred
 *
 */
public class DisplayBuilderImplTest extends TestCase {

	public void testSetText() {
		DisplayBuilder builder = new DisplayBuilderImpl();
		builder.setText("textValue");
		
		Display display = builder.build();
		assertEquals(DisplayType.TEXT, display.getType());
		assertEquals("textValue", display.getStringValue());
	}
	
	public void testSetHtml() {
		DisplayBuilder builder = new DisplayBuilderImpl();
		builder.setHtml("htmlValue");
		
		Display display = builder.build();
		assertEquals(DisplayType.HTML, display.getType());
		assertEquals("htmlValue", display.getStringValue());
	}
	

	public void testSetParagraph() {
		DisplayBuilder builder = new DisplayBuilderImpl();
		builder.setParagraph("paragraphValue");
		
		Display display = builder.build();
		assertEquals(DisplayType.PARAGRAPH, display.getType());
		assertEquals("paragraphValue", display.getStringValue());
	}
	
	public void testSetImage() {
		DisplayBuilder builder = new DisplayBuilderImpl();

		
		String url = "/url";
		RestMethod method = RestMethod.GET;
		builder.setImage(url, method);
		
		Display display = builder.build();
		assertEquals(DisplayType.IMAGE, display.getType());
		
		RestAction action = display.getAction();
		assertEquals("/url", action.getUrl());
		assertEquals(RestMethod.GET, action.getMethod());
	}
	
	public void testSetVideo() {
		DisplayBuilder builder = new DisplayBuilderImpl();

		
		String url = "/url";
		RestMethod method = RestMethod.GET;
		RestActionBuilder actionBuilder = builder.setVideo(url, method);
		assertNotNull(actionBuilder);
		
		Display display = builder.build();
		assertEquals(DisplayType.VIDEO, display.getType());
		
		RestAction action = display.getAction();
		assertEquals("/url", action.getUrl());
		assertEquals(RestMethod.GET, action.getMethod());
	}
}
