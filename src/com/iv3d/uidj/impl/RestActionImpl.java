/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package com.iv3d.uidj.impl;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.iv3d.uidj.RestAction;
import com.iv3d.uidj.RestMethod;
import com.iv3d.uidj.RestParam;

@JsonInclude(Include.NON_NULL)
public final class RestActionImpl implements RestAction {
	private String url;
	private RestMethod method;
	private List<RestParam> params;
	private String body;
	private String id;
	
	public RestActionImpl() {
		params = new ArrayList<>();
	}

	public final RestActionImpl setUrl(String url) {
		this.url = url;
		return this;
	}

	public final String getUrl() {
		return url;
	}

	public final RestActionImpl setMethod(RestMethod method) {
		this.method = method;
		return this;
	}

	public final RestMethod getMethod() {
		return method;
	}

	public final RestActionImpl addParam(String key, String value) {
		params.add(new RestParamImpl(key, value));
		return this;
	}

	@Override
	public final RestParam[] getParams() {
		if(params.isEmpty()) {
			return null;
		}
		return params.toArray(new RestParamImpl[0]);
	}

	public final RestAction setBody(String body) {
		this.body = body;
		return this;
	}

	@Override
	public String getBody() {
		return body==null?null:body;
	}
	
	public final void setParams(RestParamImpl[] params) {
		this.params.clear();
		this.params.addAll(Arrays.asList(params));
	}

	public final RestActionImpl setId(String id) {
		this.id = id;
		return this;
	}

	@Override
	public String getId() {
		return id==null?null:id;
	}

	@Override
	public String[] validate() {
		ArrayList<String> arrayList = new ArrayList<>();
		validateUrl(arrayList);
		validateMethod(arrayList);
		return arrayList.toArray(new String[]{});
	}

	private void validateMethod(ArrayList<String> arrayList) {
		if(method==null) {
			arrayList.add("action.method not specified.");
		}
	}

	private void validateUrl(ArrayList<String> arrayList) {
		if(url==null) {			
			arrayList.add("action.url not specified.");
		} else {
			try {
				new URI(url);
		    } catch (URISyntaxException e) {
		    	String message = String.format("action.url '%s' not in valid format.", url);
				arrayList.add(message);
		    }
		}
	}

	
}
