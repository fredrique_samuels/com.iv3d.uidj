/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.uidj.impl;

import java.util.ArrayList;
import java.util.List;

import com.iv3d.uidj.RestAction;
import com.iv3d.uidj.RestActionBuilder;
import com.iv3d.uidj.RestMethod;
import com.iv3d.uidj.RestParam;

/**
 * @author fred
 *
 */
public class RestActionBuilderImpl implements RestActionBuilder {

	private final String url;
	private final RestMethod method;
	private final List<RestParam> params;
	private String body;

	/**
	 * @param string
	 * @param post
	 */
	public RestActionBuilderImpl(String url, RestMethod method) {
		this.url = url;
		this.method = method;
		this.params = new ArrayList<>();
	}

	/* (non-Javadoc)
	 * @see com.iv3d.uidj.RestActionBuilder#build()
	 */
	@Override
	public RestAction build() {
		RestActionImpl action = new RestActionImpl();
		action.setUrl(url);
		action.setMethod(method);
		action.setBody(body);
		for(RestParam p : params) {action.addParam(p.getName(),  p.getValue());}
		return action;
	}

	public RestActionBuilder setBody(String body) {
		this.body = body;
		return this;
	}

	public RestActionBuilder addParam(String name, String value) {
		RestParamImpl e = new RestParamImpl(name, value);
		params.add(e);
		return this;
	}

}
