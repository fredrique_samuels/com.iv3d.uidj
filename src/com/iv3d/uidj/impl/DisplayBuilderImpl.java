/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.uidj.impl;

import com.iv3d.uidj.DataType;
import com.iv3d.uidj.Display;
import com.iv3d.uidj.DisplayBuilder;
import com.iv3d.uidj.DisplayType;
import com.iv3d.uidj.RestActionBuilder;
import com.iv3d.uidj.RestMethod;

/**
 * @author fred
 *
 */
public class DisplayBuilderImpl implements DisplayBuilder {

	private String stringValue;
	private DataType<Display> type;
	private RestActionBuilder actionBuilder;

	/**
	 * @param get
	 */
	public DisplayBuilderImpl() {
	}

	/* (non-Javadoc)
	 * @see com.iv3d.uidj.DisplayBuilder#build()
	 */
	@Override
	public Display build() {
		DisplayImpl display = new DisplayImpl();
		display.setType(type);
		display.setStringValue(stringValue);
		if(actionBuilder!=null) {
			display.setAction(actionBuilder.build());
		}
		return display;
	}

	/* (non-Javadoc)
	 * @see com.iv3d.uidj.DisplayBuilder#setText(java.lang.String)
	 */
	@Override
	public void setText(String value) {
		clear();
		type = DisplayType.TEXT;
		stringValue = value;
	}

	/**
	 * 
	 */
	private void clear() {
		type = null;
		stringValue = null;
		this.actionBuilder = null;
	}

	/* (non-Javadoc)
	 * @see com.iv3d.uidj.DisplayBuilder#setHtml(java.lang.String)
	 */
	@Override
	public void setHtml(String html) {
		clear();
		type = DisplayType.HTML;
		stringValue = html;
	}

	/* (non-Javadoc)
	 * @see com.iv3d.uidj.DisplayBuilder#setParagraph(java.lang.String)
	 */
	@Override
	public void setParagraph(String paragraph) {
		clear();
		type = DisplayType.PARAGRAPH;
		stringValue = paragraph;
	}

	/* (non-Javadoc)
	 * @see com.iv3d.uidj.DisplayBuilder#setImage(java.lang.String, com.iv3d.uidj.RestMethod)
	 */
	@Override
	public RestActionBuilder setImage(String url, RestMethod method) {
		clear();
		type = DisplayType.IMAGE;
		actionBuilder = new RestActionBuilderImpl(url, method);
		return actionBuilder;
	}

	/* (non-Javadoc)
	 * @see com.iv3d.uidj.DisplayBuilder#setVideo(java.lang.String, com.iv3d.uidj.RestMethod)
	 */
	@Override
	public RestActionBuilder setVideo(String url, RestMethod method) {
		clear();
		type = DisplayType.VIDEO;
		actionBuilder = new RestActionBuilderImpl(url, method);
		return actionBuilder;
	}

	
}
