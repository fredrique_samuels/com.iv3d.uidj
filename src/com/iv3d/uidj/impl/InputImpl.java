package com.iv3d.uidj.impl;

/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.iv3d.uidj.DataType;
import com.iv3d.uidj.Input;
import com.iv3d.uidj.Option;
import com.iv3d.uidj.impl.json.DataTypeJsonSerializer;
import com.iv3d.uidj.impl.json.InputTypeJsonDeserializer;

public class InputImpl extends AbstractComponentParam implements Input {
	private final List<OptionImpl> options;

    @JsonSerialize(using = DataTypeJsonSerializer.class)
    @JsonDeserialize(using = InputTypeJsonDeserializer.class)
	private DataType<Input> type;
	
	/**
	 * 
	 */
	public InputImpl() {
		options = new ArrayList<>();
	}

	/**
	 * @param id
	 * @param value
	 */
	public void addOption(String id, String value) {
		this.options.add(new OptionImpl(id, value));
	}

	/* (non-Javadoc)
	 * @see com.iv3d.uidj.Input#getOptions()
	 */
	@Override
	public Option[] getOptions() {
		if(options.size()==0) {
			return null;
		}
		return options.toArray(new OptionImpl[0]);
	}
	
	public void setType(DataType<Input> type) {
		this.type = type;
	}

	public DataType<Input> getType() {
		return type;
	}
}
