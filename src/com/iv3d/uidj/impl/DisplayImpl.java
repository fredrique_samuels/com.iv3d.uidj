/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.uidj.impl;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.iv3d.uidj.DataType;
import com.iv3d.uidj.Display;
import com.iv3d.uidj.RestAction;
import com.iv3d.uidj.impl.json.ActionJsonDeserializer;
import com.iv3d.uidj.impl.json.DataTypeJsonSerializer;
import com.iv3d.uidj.impl.json.DisplayTypeJsonDeserializer;

/**
 * @author fred
 *
 */
public class DisplayImpl extends AbstractComponentParam implements Display {
	
	@JsonSerialize(using = DataTypeJsonSerializer.class)
    @JsonDeserialize(using = DisplayTypeJsonDeserializer.class)
	private DataType<Display> type;
	private String stringValue;
	
	@JsonDeserialize(contentUsing=ActionJsonDeserializer.class, as=RestActionImpl.class)
	private RestAction action;

	/**
	 * @param type
	 */
	public void setType(DataType<Display> type) {
		this.type = type;
	}

	/* (non-Javadoc)
	 * @see com.iv3d.uidj.Display#getType()
	 */
	@Override
	public DataType<Display> getType() {
		return type;
	}

	/**
	 * @param string
	 */
	public void setStringValue(String value) {
		this.stringValue = value;
	}
	
	public String getStringValue() {
		return stringValue;
	}

	/**
	 * @param action
	 */
	public void setAction(RestAction action) {
		this.action = action;
	}

	public RestAction getAction() {
		return action;
	}
	
}
