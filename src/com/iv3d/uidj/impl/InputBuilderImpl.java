/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.uidj.impl;

import java.util.ArrayList;
import java.util.List;

import com.iv3d.uidj.DataType;
import com.iv3d.uidj.Input;
import com.iv3d.uidj.InputBuilder;

/**
 * @author fred
 *
 */
public class InputBuilderImpl implements InputBuilder {

	private final DataType<Input> type;
	private final String id;
	private final List<OptionImpl> options;
	private String name;
	private boolean hidden;

	/**
	 * @param string
	 * @param text
	 */
	public InputBuilderImpl(String id, DataType<Input> type) {
		this.id = id;
		this.type = type;
		this.hidden = false;
		this.options = new ArrayList<OptionImpl>();
	}

	/* (non-Javadoc)
	 * @see com.iv3d.uidj.InputBuilder#build()
	 */
	@Override
	public Input build() {
		InputImpl input = new InputImpl();
		input.setId(id);
		input.setType(type);
		input.setName(name);
		for(OptionImpl op : options) {input.addOption(op.getId(), op.getValue());}
		if(hidden) {
			input.hide();
		} else {
			input.show();
		}
		
		return input;
	}

	/* (non-Javadoc)
	 * @see com.iv3d.uidj.InputBuilder#setName(java.lang.String)
	 */
	@Override
	public InputBuilder setName(String name) {
		this.name = name;
		return this;
	}

	/* (non-Javadoc)
	 * @see com.iv3d.uidj.InputBuilder#setHidden()
	 */
	@Override
	public InputBuilder setHidden() {
		hidden = true;
		return this;
	}

	/* (non-Javadoc)
	 * @see com.iv3d.uidj.InputBuilder#addOption(java.lang.String, java.lang.String)
	 */
	@Override
	public InputBuilder addOption(String name, String value) {
		options.add(new OptionImpl(name, value));
		return this;
	}

}
