package com.iv3d.uidj;

public interface RestParam {
	String getName();
	String getValue();
}
