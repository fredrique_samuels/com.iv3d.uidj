Roger Federer
Country (sports)	  Switzerland
Residence	Bottmingen, Switzerland[1]
Born	8 August 1981 (age 34)
Basel, Switzerland
Height	1.85 m (6 ft 1 in)[2]
Turned pro	1998
Plays	Right-handed (one-handed backhand)
Prize money	US$ 97,958,429
Official website	rogerfederer.com

Roger Federer (German: [ˈfeːdərər] born 8 August 1981) is a Swiss professional tennis player who is currently ranked world No. 3 by the Association of Tennis Professionals (ATP).[3] His accomplishments in professional tennis cause him to be regarded by many as the greatest tennis player of all time.[a] Federer turned professional in 1998 and has been continuously ranked in the top 10 since October 2002.

=======================

Rafael "Rafa" Nadal Parera (Catalan: [rəfəˈɛɫ nəˈðaɫ pəˈɾeɾə], Spanish: [rafaˈel naˈðal paˈɾeɾa]; born 3 June 1986) is a Spanish professional tennis player currently ranked world No. 5.[3] He is widely regarded as the greatest clay-court player in history;[a] due to his dominance and success on the surface, he has been titled "The King of Clay".[b] His evolution into an all-court threat has established him as one of the greatest players in tennis history,[c] with some considering Nadal to be the greatest player of all time.[28]

=======================

Efren Manalang Reyes, OLD, PLH (born August 26, 1954), popularly known as Efren "Bata" Reyes and nicknamed 'The Magician', is a Filipino professional pool player. A winner of over 70 international titles[citation needed], Reyes is the first man in history to win World Championships in two different disciplines in pool. Besides his World titles he is also a 14-time Derby City Classic champion, a two-time World Cup champion, and a two-time World Pool League champion. By defeating Earl Strickland in the inaugural Color of Money event in 1996, Reyes took home the largest single event purse in pool history.[1] Many analysts, fans, and current and former players consider Reyes to be the greatest pool player of all time.[2][3]

=======================

Earl "The Pearl" Strickland (born June 8, 1961 in Roseboro, North Carolina) is an American professional pool player who is considered one of the best nine-ball players of all time. He has won numerous championship titles and, in 2006, was inducted into the Billiard Congress of America's Hall of Fame.[1] He is also known as one of the sport's most controversial players for his outspoken views and his sometimes volatile behavior at tournaments.